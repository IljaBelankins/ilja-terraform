# Using terraform
To use Terraform:
First, create a .tf file for app, structured appropriately and containing relevant resources

then

'''
terraform init
terraform plan
terraform apply
'''