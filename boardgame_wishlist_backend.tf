resource "heroku_app" "boardgame-wishlist-backend" {
  name   = "boardgamewishlist"
  region = "eu"
}

resource "heroku_addon" "boardgame-wishlist-backend-database" {
    app = "boardgamewishlist"
    plan = "heroku-postgresql:hobby-dev"
}