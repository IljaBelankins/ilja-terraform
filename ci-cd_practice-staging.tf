resource "heroku_app" "ci-cd-practice-staging" {
  name   = "ci-cd-practice-staging"
  region = "eu"
}

resource "heroku_addon" "ci-cd-practice-staging-database" {
    app = "ci-cd-practice-staging"
    plan = "heroku-postgresql:hobby-dev"
}