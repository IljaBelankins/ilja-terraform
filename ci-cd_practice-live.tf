resource "heroku_app" "ci-cd-practice-live" {
  name   = "ci-cd-practice-live"
  region = "eu"
}

resource "heroku_addon" "ci-cd-practice-live-database" {
    app = "ci-cd-practice-live"
    plan = "heroku-postgresql:hobby-dev"
}